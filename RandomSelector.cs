using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A composite that finds a random successful child.</summary>
	/// <remarks>
	/// A <c>RandomSelector</c> behavior runs through each of its children in randomized order, looking for the first
	/// successful result. The order of behaviors is shuffled each time it is started with <c>Begin</c>. Like its
	/// superclass <c><see cref="Selector" /></c>, if a child returns <c>Failure</c>, it moves on to the next child. If
	/// it has no remaining children, it returns <c>Failure</c>. If any child returns <c>Success</c>, it immediately
	/// returns <c>Success</c> as well. A common use for a <c>RandomSelector</c> is to choose what kind of behavior to run
	/// when there is no clear list of priorities or when nondeterminstic behavior is important.
	/// </remarks>
	public class RandomSelector : Selector
	{
		private static Random rnd = new Random();

		/// <summary>Shuffles a list of behavior nodes in-place.</summary>
		/// <remarks>
		/// The list will be shuffled in-place using the Fisher-Yates algorithm.
		/// </remarks>
		/// <param name="list">The set of behaviors to randomize.</param>
		public static void Randomize(IList<IBehaviorTree> list)
		{
			int len = list.Count;
			for (int i = 0; i < len; i++) {
				int j = rnd.Next(i, len);
				IBehaviorTree temp = list[i];
				list[i] = list[j];
				list[j] = temp;
			}
		}

		/// <summary>
		/// Constructs a <c>RandomSelector</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// This <c>RandomSelector</c> constructor does not create a copy of the list of children it receives. Callers
		/// should not mutate the list after the composite is constructed and should be aware that <c>RandomSelector</c>
		/// will periodically modify it.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public RandomSelector(IList<IBehaviorTree> children)
			: base(children)
		{
			// initializers only
		}

		/// <summary>
		/// Constructs a <c>RandomSelector</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// If invoked variadically, this <c>RandomSelector</c> constructor creates a new list object and does not share it
		/// with the caller. If invoked with an array object, this is not the case, and callers should not mutate the array
		/// after the composite is constructed and should be aware that <c>RandomSelector</c> will periodically modify it.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public RandomSelector(params IBehaviorTree[] children)
			: base(children)
		{
			// initializers only
		}

		/// <summary>
		/// Starts or restarts the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Begin" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		public override void Begin(IDictionary<string, Object> context)
		{
			RandomSelector.Randomize(children);
			base.Begin(context);
		}
	}
}
