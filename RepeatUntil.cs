using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A decorator that re-executes its child until it returns a desired status.</summary>
	/// <remarks>
	/// A <c>RepeatUntil</c> decorator runs its child repeatedly until it returns a desired value. When the child
	/// completes, it is restarted unless the return value matches the <c>status</c> parameter to the constructor.
	/// When this happens, <c>RepeatUntil</c> will also return that status. This is useful, for example, if a behavior
	/// should be retried until it succeeds.
	/// </remarks>
	public class RepeatUntil : Decorator
	{
		private BehaviorResult status;
		private bool mustBegin;

		/// <summary>
		/// Constructs a <c>RepeatUntil</c> node that runs <c>child</c> repeatedly until it returns <c>status</c>.
		/// </summary>
		/// <remarks>
		/// Constructs a <c>RepeatUntil</c> node that runs <c>child</c> repeatedly until it returns <c>status</c>.
		/// </remarks>
		/// <param name="child">The child behavior to execute.</param>
		/// <param name="status">The desired return value of the child.</param>
		public RepeatUntil(IBehaviorTree child, BehaviorResult status)
			: base(child)
		{
			this.status = status;
			mustBegin = false;
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public override BehaviorResult Process(IDictionary<string, Object> context)
		{
			if (mustBegin) {
				mustBegin = false;
				Child.Begin(context);
			}
			BehaviorResult childResult = Child.Process(context);
			if (childResult == status) {
				return childResult;
			} else if (childResult != BehaviorResult.Running) {
				mustBegin = true;
			}
			return BehaviorResult.Running;
		}
	}
}
