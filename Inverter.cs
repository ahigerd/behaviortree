using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A decorator that inverts its child's return value.</summary>
	/// <remarks>
	/// An <c>Inverter</c> decorator passes through control to a child behavior, but when that behavior completes, it
	/// returns the opposite value. This is useful, for example, if the child represents some sort of boolean state check
	/// and the parent sequence needs to proceed if the state is false instead of true.
	/// </remarks>
	public class Inverter : Decorator
	{
		/// <summary>
		/// Constructs an <c>Inverter</c> node that wraps <c>child</c>.
		/// </summary>
		/// <remarks>
		/// Constructs an <c>Inverter</c> node that wraps <c>child</c>.
		/// </remarks>
		/// <param name="child">The child behavior to execute.</param>
		public Inverter(IBehaviorTree child)
			: base(child)
		{
			// initializers only
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public override BehaviorResult Process(IDictionary<string, Object> context)
		{
			BehaviorResult childResult = Child.Process(context);
			if (childResult == BehaviorResult.Success) {
				return BehaviorResult.Failure;
			} else if (childResult == BehaviorResult.Failure) {
				return BehaviorResult.Success;
			}
			return BehaviorResult.Running;
		}
	}
}
