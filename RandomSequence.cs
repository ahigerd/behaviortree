using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A composite that runs through its children in random order.</summary>
	/// <remarks>
	/// A <c>RandomSequence</c> behavior runs through each of its children in randomized order. The order of behaviors is
	/// shuffled each time it is started with <c>Begin</c>. Like its superclass <c><see cref="Sequence" /></c>, it returns
	/// <c>Success</c> after all of its children have returned <c>Success</c>. If a child returns <c>Failure</c>, it
	/// stops and returns <c>Failure</c> as well. A common use for a <c>RandomSequence</c> is to execute a set of tasks
	/// that need to be accomplished when the ordering is unimportant and nondeterministic behavior is desired.
	/// </remarks>
	public class RandomSequence : Sequence
	{
		/// <summary>
		/// Constructs a <c>RandomSequence</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// This <c>RandomSequence</c> constructor does not create a copy of the list of children it receives. Callers
		/// should not mutate the list after the composite is constructed and should be aware that <c>RandomSequence</c>
		/// will periodically modify it.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public RandomSequence(IList<IBehaviorTree> children)
			: base(children)
		{
			// initializers only
		}

		/// <summary>
		/// Constructs a <c>RandomSequence</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// If invoked variadically, this <c>RandomSequence</c> constructor creates a new list object and does not share it
		/// with the caller. If invoked with an array object, this is not the case, and callers should not mutate the array
		/// after the composite is constructed and should be aware that <c>RandomSequence</c> will periodically modify it.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public RandomSequence(params IBehaviorTree[] children)
			: base(children)
		{
			// initializers only
		}

		/// <summary>
		/// Starts or restarts the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Begin" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		public override void Begin(IDictionary<string, Object> context)
		{
			RandomSelector.Randomize(children);
			base.Begin(context);
		}
	}
}

