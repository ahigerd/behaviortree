using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A behavior tree node that wraps around another node.</summary>
	/// <remarks>
	/// <c>Decorator</c> is an abstract behavior tree node that provides common functionality for decorator nodes. A
	/// decorator is a type of behavior that wraps around another behavior to control some aspect of its execution, such
	/// as altering its return value or executing it multiple times.
	/// </remarks>
	public abstract class Decorator : IBehaviorTree
	{
		private IBehaviorTree child;

		/// <summary>
		/// Constructs a <c>Decorator</c> node that wraps <c>child</c>.
		/// </summary>
		/// <remarks>
		/// Constructs a <c>Decorator</c> node that wraps <c>child</c>.
		/// </remarks>
		/// <param name="child">The child behavior to execute.</param>
		public Decorator(IBehaviorTree child)
		{
			this.child = child;
		}

		/// <summary>The behavior being decorated.</summary>
		/// <value>The <c>IBehaviorTree</c> object passed to the constructor.</value>
		/// <remarks>A <c>Decorator</c>'s selected child is decided upon construction and cannot be changed.</remarks>
		protected IBehaviorTree Child
		{
			get {
				return child;
			}
		}

		/// <summary>
		/// Starts or restarts the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Begin" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		public void Begin(IDictionary<string, Object> context)
		{
			child.Begin(context);
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public abstract BehaviorResult Process(IDictionary<string, Object> context);
	}
}
