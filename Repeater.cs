using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A decorator that continually re-executes its child.</summary>
	/// <remarks>
	/// A <c>Repeater</c> decorator always returns <c>Running</c>. When its child completes, it unconditionally restarts
	/// it on the next tick. This is useful, for example, as the top-level node in a tree to continually run an actor's
	/// AI at all times.
	/// </remarks>
	public class Repeater : Decorator
	{
		private bool mustBegin;

		/// <summary>
		/// Constructs a <c>Repeater</c> node that runs <c>child</c> repeatedly.
		/// </summary>
		/// <remarks>
		/// Constructs a <c>Repeater</c> node that runs <c>child</c> repeatedly.
		/// </remarks>
		/// <param name="child">The child behavior to execute.</param>
		public Repeater(IBehaviorTree child)
			: base(child)
		{
			mustBegin = false;
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// Always returns <c><see cref="BehaviorResult.Running" /></c>.
		/// </returns>
		public override BehaviorResult Process(IDictionary<string, Object> context)
		{
			if (mustBegin) {
				mustBegin = false;
				Child.Begin(context);
			}
			BehaviorResult childResult = Child.Process(context);
			if (childResult != BehaviorResult.Running) {
				mustBegin = true;
			}
			return BehaviorResult.Running;
		}
	}
}
