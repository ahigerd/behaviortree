using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	// A Sequence behavior runs through each of its children in order.
	// After all of its children have returned Success, it also returns Success.
	// If any child returns Failure, it immediately returns Failure as well.
	/// <summary>A composite that runs through its children in order.</summary>
	/// <remarks>
	/// A <c>Sequence</c> behavior runs through each of its children in order. It returns <c>Success</c> after all of its
	/// children have returned <c>Success</c>. If a child returns <c>Failure</c>, it stops and returns <c>Failure</c> as
	/// well. A common use for a <c>Sequence</c> is to execute a set of tasks that have to be done in order, where the
	/// operation cannot proceed further if any step fails.
	/// </remarks>
	public class Sequence : SequentialComposite
	{
		/// <summary>
		/// Constructs a <c>Sequence</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// This <c>Sequence</c> constructor does not create a copy of the list of children it receives. Callers should not
		/// mutate the list after the composite is constructed.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public Sequence(IList<IBehaviorTree> children)
			: base(children)
		{
			// initializers only
		}

		/// <summary>
		/// Constructs a <c>Sequence</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// If invoked variadically, this <c>Sequence</c> constructor creates a new list object and does not share it with
		/// the caller. If invoked with an array object, this is not the case, and callers should not mutate the array after
		/// the composite is constructed.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public Sequence(params IBehaviorTree[] children)
			: this((IList<IBehaviorTree>)children)
		{
			// chained constructor only
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public override BehaviorResult Process(IDictionary<string, Object> context)
		{
			while (CurrentChild != null) {
				BehaviorResult childResult = CurrentChild.Process(context);
				if (childResult == BehaviorResult.Success) {
					SelectNextChild(context);
				} else {
					return childResult;
				}
			}
			return BehaviorResult.Success;
		}
	}
}
