XBUILD := xbuild

MDOC_FLAGS :=

ifneq ($(MONO_PATH),)
	MDOC_FLAGS := $(foreach dir,$(subst :, ,$(MONO_PATH)),-L $(dir))
endif

debug: bin/Debug/BehaviorTree.dll

release: bin/Release/BehaviorTree.dll

bin/Debug/BehaviorTree.dll: $(wildcard *.cs)
	$(XBUILD) /property:Configuration=Debug /target:BehaviorTree behaviortree.sln

bin/Release/BehaviorTree.dll: $(wildcard *.cs)
	$(XBUILD) /property:Configuration=Release /target:BehaviorTree behaviortree.sln

bin/Debug/behaviortree.xml: bin/Debug/BehaviorTree.dll

docs/%.xml: %.xml
	mkdir -p docs
	cp $< $@

doc: bin/Debug/behaviortree.xml docs/ns-BehaviorTree.xml docs/index.xml
	mdoc update -o docs bin/Debug/BehaviorTree.dll --delete -i bin/Debug/behaviortree.xml $(MDOC_FLAGS)
	mkdir -p docs-html
	mdoc export-html -o docs-html docs

clean:
	rm -rf docs docs-html bin obj

watch: doc
	while fswatch -1 $(CURDIR); do $(MAKE) doc; done
