using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>
	/// A behavior tree node with prioritized children that can interrupt other behaviors.
	/// </summary>
	/// <remarks>
	/// An <c>InterruptSelector</c> behavior runs through each of its children in order every tick. If a child returns
	/// <c>Failure</c>, it is reset, and <c>InterruptSelector</c> moves on to the next child. If the child does not return
	/// <c>Failure</c>, then all later children in the list are reset, and <c>InterruptSelector</c> returns the same value
	/// as the last child executed. If all children fail, <c>InterruptSelector</c> returns <c>Failure</c> as well.
	/// <para>
	/// The intended use of an <c>InterruptSelector</c> is to provide functionality similar to a <see cref="Selector" />
	/// while allowing higher-priority behaviors to preempt lower-priority ones that might still be running.
	/// </para>
	/// </remarks>
	public class InterruptSelector : IBehaviorTree
	{
		private IList<IBehaviorTree> children;
		private bool[] reset;

		/// <summary>
		/// Constructs an <c>InterruptSelector</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// This <c>InterruptSelector</c> constructor does not create a copy of the list of children it receives. Callers
		/// should not mutate the list after the composite is constructed.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public InterruptSelector(IList<IBehaviorTree> children)
		{
			this.children = children;
			reset = new bool[children.Count];
		}

		/// <summary>
		/// Constructs an <c>InterruptSelector</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// If invoked variadically, this <c>InterruptSelector</c> constructor creates a new list object and does not share
		/// it with the caller. If invoked with an array object, this is not the case, and callers should not mutate the
		/// array after the composite is constructed.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public InterruptSelector(params IBehaviorTree[] children)
			: this((IList<IBehaviorTree>)children)
		{
			// chained constructor only
		}

		/// <summary>
		/// Starts or restarts the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Begin" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		public void Begin(IDictionary<string, Object> context)
		{
			for (int i = 0; i < children.Count; i++) {
				reset[i] = true;
			}
		}

		private void ResetAllLater(int index)
		{
			for (int i = index + 1; i < children.Count; i++) {
				reset[index] = true;
			}
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public BehaviorResult Process(IDictionary<string, Object> context)
		{
			int i = 0;
			while (i < children.Count) {
				IBehaviorTree child = children[i];
				if (reset[i]) {
					child.Begin(context);
					reset[i] = false;
				}
				BehaviorResult childResult = child.Process(context);
				if (childResult == BehaviorResult.Failure) {
					reset[i] = true;
					i++;
				} else {
					ResetAllLater(i);
					return childResult;
				}
			}
			return BehaviorResult.Failure;
		}
	}
}
