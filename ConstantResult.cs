using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A decorator that ignores its child's return value, substituting a fixed value of its own.</summary>
	/// <remarks>
	/// A <c>ConstantResult</c> decorator passes through control to a child behavior, but when that behavior completes,
	/// it returns the value set in the constructor. This is useful, for example, to signal <c>Success</c> to a parent
	/// sequence if it's not important if the child fails.
	/// </remarks>
	public class ConstantResult : Decorator
	{
		private BehaviorResult value;

		/// <summary>
		/// Constructs a <c>ConstantResult</c> node that wraps <c>child</c> and returns the specified <c>value</c>.
		/// </summary>
		/// <remarks>
		/// Constructs a <c>ConstantResult</c> node that wraps <c>child</c> and returns the specified <c>value</c>.
		/// </remarks>
		/// <param name="child">The child behavior to execute.</param>
		/// <param name="value">The result to return when the child completes.</param>
		public ConstantResult(IBehaviorTree child, BehaviorResult value)
			: base(child)
		{
			this.value = value;
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public override BehaviorResult Process(IDictionary<string, Object> context)
		{
			BehaviorResult result = Child.Process(context);
			if (result == BehaviorResult.Running) {
				return BehaviorResult.Running;
			}
			return this.value;
		}
	}
}
