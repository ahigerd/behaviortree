using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>The result of processing a single tick of a behavior.</summary>
	/// <remarks>
	/// A <c>BehaviorResult</c> indicates the return status of a single tick of a behavior node.
	/// <para>
	/// A behavior that returns <c>Running</c> has additional work that it will do next tick. A behavior that has
	/// returned <c>Success</c> or <c>Failure</c> has completed and wishes to communicate the specified result to its
	/// parent. Callers should not invoke the <c><see cref="IBehaviorTree.Process" /></c> method on a completed behavior
	/// until it has been restarted using the <c><see cref="IBehaviorTree.Begin" /></c> method.
	/// </para>
	/// </remarks>
	public enum BehaviorResult
	{
		/// <summary>The behavior has not yet completed.</summary>
		Running,
		/// <summary>The behavior signaled success or a true value.</summary>
		Success,
		/// <summary>The behavior signaled failure or a false value.</summary>
		Failure
	}

	/// <summary>
	/// Defines the methods necessary to start and process a behavior in a hierarchical tree.
	/// </summary>
	/// <remarks>
	/// <c>IBehaviorTree</c> provides an interface for nodes in a behavior tree. Most nodes can be divided into one of
	/// three types: composites, decorators, and leaves. Composites and decorators contain one or more child nodes, which
	/// they delegate to. Leaves generally perform an action or evaluate a condition, returning success or failure, which
	/// parent nodes can use to make decisions.
	/// <para>
	/// Callers are responsible for invoking the lifecycle methods in the right order. The caller must invoke
	/// <c><see cref='Begin' /></c> before invoking <c><see cref='Process' /></c> and it must observe the return value of
	/// <c>Process</c>. After <c>Process</c> returns <c><see cref='BehaviorResult.Success' /></c> or
	/// <c><see cref='BehaviorResult.Failure' /></c>, the caller must reinvoke <c>Begin</c> before calling <c>Process</c>
	/// again.
	/// </para>
	/// <para>
	/// Implementations of this interface must be able to handle the <c>Begin</c> method being called at any time except
	/// during the execution of the <c>Begin</c> or <c>Process</c> methods, abandoning any internal state and
	/// reinitializing itself to run with a new context.
	/// </para>
	/// <para>
	/// The <c>context</c> parameter passed to <c>Begin</c> and <c>Process</c> requires special attention.
	/// (TODO: write this)
	/// </para>
	/// </remarks>
	public interface IBehaviorTree
	{
		/// <summary>
		/// Starts or restarts the behavior.
		/// </summary>
		/// <remarks>
		/// The <c>Begin</c> method should be invoked before <c>Process</c> is called for the first time, and then again
		/// after <c>Process</c> returns <c>Success</c> or <c>Failure</c> before invoking it again. It may also be called at
		/// any time to interrupt the behavior and restart it with a fresh context.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// See the Remarks section for <c><see cref="IBehaviorTree" /></c> for more information.
		/// </param>
		void Begin(IDictionary<string, Object> context);

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// The <c>Process</c> method is responsible for performing the work of the behavior. It should return
		/// <c>Running</c> if it is unable to complete the task within a single tick. Once the work is complete or can no
		/// longer proceed, it should return <c>Success</c> or <c>Failure</c> to communicate the desired value to its
		/// parent.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// See the Remarks section for <c><see cref="IBehaviorTree" /></c> for more information.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		BehaviorResult Process(IDictionary<string, Object> context);
	}
}
