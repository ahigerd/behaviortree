using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A behavior tree node that contains multiple children.</summary>
	/// <remarks>
	/// <c>SequentialComposite</c> is an abstract behavior tree node that provides common functionality for composite
	/// nodes that iterate over an arbitrary number of children in a predefined order.
	/// </remarks>
	/// <seealso cref='Sequence' />
	/// <seealso cref='Selector' />
	/// <seealso cref='RandomSequence' />
	/// <seealso cref='RandomSelector' />
	public abstract class SequentialComposite : IBehaviorTree
	{
		/// <summary>The child nodes to be run by the composite.</summary>
		/// <remarks>
		/// By default, <c>children</c> is a shared reference to the list passed to the constructor, and mutations to the
		/// list will affect the caller. Subclasses that mutate the list should document this behavior.
		/// </remarks>
		protected IList<IBehaviorTree> children;

		/// <summary>The current position in the list of children.</summary>
		/// <remarks>
		/// While this is mutable, subclasses that modify the value of <c>index</c> instead of using <c>SelectNextChild</c>
		/// must take care of invoking <c>Begin</c> on the newly-selected child.
		/// </remarks>
		protected int index;

		/// <summary>
		/// Constructs a <c>SequentialComposite</c> node that will iterate over a list of children.
		/// </summary>
		/// <remarks>
		/// The <c>SequentialComposite</c> constructor does not create a copy of the list of children it receives. Some
		/// subclasses, such as <c><see cref="RandomSequence" /></c>, may mutate the list. Callers should not mutate the
		/// list after the composite is constructed, unless a subclass's documentation states that it makes a copy of its
		/// own.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be iterated over.
		/// </param>
		public SequentialComposite(IList<IBehaviorTree> children)
		{
			this.children = children;
		}

		/// <summary>
		/// Starts or restarts the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Begin" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		public virtual void Begin(IDictionary<string, Object> context)
		{
			index = 0;
			if (children.Count > 0) {
				children[0].Begin(context);
			}
		}

		/// <summary>
		/// The child node that should be processed next.
		/// </summary>
		/// <value>
		/// One of the <c>IBehaviorTree</c> objects from the <c>children</c> list.
		/// </value>
		/// <remarks>
		/// This property returns <c>null</c> if all of the children have been evaluated.
		/// </remarks>
		protected IBehaviorTree CurrentChild
		{
			get {
				if (index >= children.Count) {
					return null;
				}
				return children[index];
			}
		}

		/// <summary>
		/// Proceeds to the next child.
		/// </summary>
		/// <remarks>
		/// <c>SelectNextChild</c> causes the composite to move on to the next child in the sequence. The <c>context</c>
		/// parameter is passed to the new child's <c>Begin</c> method.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		protected void SelectNextChild(IDictionary<string, Object> context)
		{
			index++;
			if (CurrentChild != null) {
				CurrentChild.Begin(context);
			}
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public abstract BehaviorResult Process(IDictionary<string, Object> context);
	}
}
