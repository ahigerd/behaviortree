BehaviorTree
============

This project provides a C# implementation of behavior trees.

*TODO: Write a better readme*

[Automatically generated documentation](http://greenmaw.com/behaviortree/) is available.

Resources
---------

[Behavior trees for AI: How they work](http://www.gamasutra.com/blogs/ChrisSimpson/20140717/221339/Behavior_trees_for_AI_How_they_work.php) by Chris Simpson
