using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A composite that finds the first successful child.</summary>
	/// <remarks>
	/// A <c>Selector</c> behavior runs through each of its children in order, looking for the first successful result.
	/// If a child returns <c>Failure</c>, it moves on to the next child. If it has no remaining children, it returns
	/// <c>Failure</c>. If any child returns <c>Success</c>, it immediately returns <c>Success</c> as well. A common use
	/// for a <c>Selector</c> is to choose what kind of behavior to run out of a prioritized list of options.
	/// </remarks>
	public class Selector : SequentialComposite
	{
		/// <summary>
		/// Constructs a <c>Selector</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// This <c>Selector</c> constructor does not create a copy of the list of children it receives. Callers should not
		/// mutate the list after the composite is constructed.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public Selector(IList<IBehaviorTree> children)
			: base(children)
		{
			// initializers only
		}

		/// <summary>
		/// Constructs a <c>Selector</c> node that will operate upon a list of children.
		/// </summary>
		/// <remarks>
		/// If invoked variadically, this <c>Selector</c> constructor creates a new list object and does not share it with
		/// the caller. If invoked with an array object, this is not the case, and callers should not mutate the array after
		/// the composite is constructed.
		/// </remarks>
		/// <param name="children">
		/// The list of child nodes to be operated upon.
		/// </param>
		public Selector(params IBehaviorTree[] children)
			: this((IList<IBehaviorTree>)children)
		{
			// chained constructor only
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public override BehaviorResult Process(IDictionary<string, Object> context)
		{
			while (CurrentChild != null) {
				BehaviorResult childResult = CurrentChild.Process(context);
				if (childResult == BehaviorResult.Failure) {
					SelectNextChild(context);
				} else {
					return childResult;
				}
			}
			return BehaviorResult.Failure;
		}
	}
}
