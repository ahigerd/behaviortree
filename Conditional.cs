using System;
using System.Collections.Generic;

namespace BehaviorTree
{
	/// <summary>A behavior tree node that invokes one of two behaviors based on a third.</summary>
	/// <remarks>
	/// <c>Conditional</c> is a behavior tree node that chooses between two possible behaviors based on the result of
	/// another behavior. If the <c>condition</c> behavior passed to the constructor returns <c>Success</c>, then the
	/// <c>onTrue</c> behavior will be run. If it returns <c>Failure</c>, then the <c>onFalse</c> behavior will be run.
	/// The return value of the <c>Conditional</c> behavior is the return value of the selected child behavior.
	/// </remarks>
	public abstract class Conditional : IBehaviorTree
	{
		private IBehaviorTree condition;
		private IBehaviorTree onTrue;
		private IBehaviorTree onFalse;
		private IBehaviorTree selected;

		/// <summary>
		/// Constructs a <c>Conditional</c> node that invokes one of two behaviors based on a third.
		/// </summary>
		/// <remarks>
		/// Constructs a <c>Conditional</c> node that invokes one of two behaviors based on a third.
		/// </remarks>
		/// <param name="condition">A child behavior that will determine which path to take.</param>
		/// <param name="onTrue">A child behavior that will be run if <c>condition</c> returns <c>Success</c>.</param>
		/// <param name="onFalse">A child behavior that will be run if <c>condition</c> returns <c>Failure</c>.</param>
		public Conditional(IBehaviorTree condition, IBehaviorTree onTrue, IBehaviorTree onFalse)
		{
			this.condition = condition;
			this.onTrue = onTrue;
			this.onFalse = onFalse;
		}

		/// <summary>
		/// Starts or restarts the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Begin" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		public void Begin(IDictionary<string, Object> context)
		{
			condition.Begin(context);
			selected = null;
		}

		/// <summary>
		/// Processes one tick of the behavior.
		/// </summary>
		/// <remarks>
		/// See <see cref="IBehaviorTree.Process" />.
		/// </remarks>
		/// <param name="context">
		/// The context for the behavior to operate upon.
		/// </param>
		/// <returns>
		/// A value that indicates the state of the behavior after the tick.
		/// </returns>
		public BehaviorResult Process(IDictionary<string, Object> context)
		{
			if (selected == null) {
				BehaviorResult result = condition.Process(context);
				if (result == BehaviorResult.Running) {
					return result;
				}
				selected = (result == BehaviorResult.Success) ? onTrue : onFalse;
				selected.Begin(context);
			}
			return selected.Process(context);
		}
	}
}
